# Git Scope #
Git Scope IntelliJ IDEA Plugin.

### What does it do? ###
Git Scope is a helpful plugin for displaying and highlighting changed code (similar to 'git diff', with several advantages)
The plugin is used as a tool window.
You simply select the target branch and see all changed files in the integrated file browser (continuously*).

Furthermore:

Line Status:
The line status of all relevant files adapts to the selected revision (branch).
What is the Line Status?: https://www.jetbrains.com/help/phpstorm/file-status-highlights.html.

Scopes:
The plugin creates a custom scope (to use inspections, search / replaces, ect)
Thus it is eg possible to carry out a global search only over the changed files!
It is useful to quickly locate and remove debug code ect before requesting a pull request.
What are Scopes?: https://www.jetbrains.com/help/phpstorm/scopes.html.

continuously* means: The integrated file browser, the line status and the custom scope automatically refresh on every git-action like checkout/commit/merge and editor-actions like modify, create or delete a file.

Summary:
1. Adds a tool window for custom diffs
2. Extends line status according to your selection
3. Adds a custom scope "Git Scope" according to your selection

### How to use ###
- Open: View > Tool Windows > Git Scope

Tool Window
- Settings / Usage
  - Select a **branch** or paste a **commit hash**
  - combine a branch with **origin-Checkbox** to see changes for remote-branch
  - check **HEAD-Checkbox** to see HEAD changes only instead
  - you will see changes in the integrated file browser immediately _(according to the performance to your computer and/or network)_

Line Status
- Line Status automatically apply

Custom Scope
- Custom Scope "Git Scope" automatically apply
  - Perform "Find in Path" / "Replace in Path" and Inspections based on your custom Scope created with this Plugin.
  - Example: STRG+SHIFT+F > Scope > Git Scope (Files) and then search something
  - Also useful for Inspections with your custom scope  


### Use case example ###
Agile teams often use branches like **master** or **integration** as "main-line" and feature branches for their development.
With the plugin it is possible to show all changes of your feature branch to the "main-line" (Example: MyFeatureBranch > master)
Youre able to diff to master (or something else) like you would do with a pull request!
Its the not the same as **Version Control > Local Changes**, because this Toolwindow shows only HEAD changes!
It is a Toolwindow of the "Compare with branch..."-Action with some more benefits!

Another Use case is to review branches of your colleagues.
Its very useful to see the changes of a "target"-branch **instantly** according to the current "source"-branch.

## Version ##
Please see plugin.xml <version>-Tag

## Install ##
Settings (STRG+ALT+S) > Plugins > "Browse repositories..." > Search: "Git Scope" > Install

OR

1. Download: Go to 'Source' > Git Scope.jar > RAW
2. Install: Settings (STRG+ALT+S) > Plugins > "Install plugin from disk..."

## Contribute ##
Please contact me through Bitbucket.