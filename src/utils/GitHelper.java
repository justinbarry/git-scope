package utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtil;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vcs.VcsException;
import com.intellij.openapi.vcs.changes.Change;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiFile;
import com.intellij.psi.impl.source.PsiFileImpl;
import com.intellij.psi.tree.IElementType;
import git4idea.GitLocalBranch;
import git4idea.GitUtil;
import git4idea.commands.Git;
import git4idea.repo.*;
import git4idea.ui.branch.GitMultiRootBranchConfig;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GitHelper {

    Project project;
    GitRepository repository;

    public GitHelper(Project project) {
        this.project = project;
    }

    public void initGitRepositoryOnStart() {

        try {

            com.intellij.openapi.editor.Editor editor = FileEditorManager.getInstance(this.project).getSelectedTextEditor();
            Document document = editor.getDocument();

            VirtualFile thisFile = FileDocumentManager.getInstance().getFile(document);

            VirtualFile repositoryRoot = GitUtil.getGitRoot(thisFile);

            if (repositoryRoot == null) {
                System.out.println("null 1");
                return;
            }
            if (GitUtil.findGitDir(repositoryRoot) == null) {
                System.out.println("null 2");
                return;
            }

            this.repository = GitRepositoryImpl.getInstance(repositoryRoot, this.project, false);

            System.out.println(this.repository.getCurrentBranch());
            utils.Notification.notify("current branch", this.repository.getCurrentBranch().toString());

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            utils.Notification.notify("Vcs Exception", e.getMessage());
        }

    }

    public Map<String, Object> getDiffList(String currentBranch, String otherBranch) {

        Collection<String> diff = null;
        try {
            diff = GitUtil.getPathsDiffBetweenRefs(Git.getInstance(), this.repository, currentBranch, otherBranch);
        } catch (VcsException e) {
            e.printStackTrace();
        }
        List<Change> changes = GitUtil.findLocalChangesForPaths(this.project, this.repository.getRoot(), diff, false);

        Map<String, Object> changedFileCollection = new HashMap<String, Object>();

        for (Change change : changes) {

            String absolutePath = change.getVirtualFile().getPath();
            String basePath = this.project.getBasePath();
            String relativePath = absolutePath.replace(basePath + "/","");
            String fileStatus = change.getFileStatus().getId();

            Map<String, String> changedFile = new HashMap<String, String>();
            changedFile.put("fileStatus", fileStatus);
            changedFile.put("relativePath", relativePath);
            changedFile.put("absolutePath", absolutePath);

            changedFileCollection.put(relativePath, changedFile);

        }

        return changedFileCollection;
    }

    public GitRepository getRepository () {
        return this.repository;
    }

    /**
     * ssh://git@bitbucket.check24.de:7999/vg/hausrat-core.git
     * ssh://git@bitbucket.check24.de:7999/vg/hausrat-desktop-check24.git
     *
     * @return
     */
    public String getOriginUrl() {

        String origin = "";

        Collection<GitRemote> remotes = this.repository.getRemotes();
        for (GitRemote remote : remotes) {
            if (remote.getName().equals("origin")) {
                List<String> origins = remote.getUrls();
                origin = origins.get(0);
            }
        }

        return origin;

    }

    public String getOrigin() {

        if (this.repository == null) return "";
        String originUrl = getOriginUrl();

        if (originUrl == null) {
            return "";
        }

        String[] originUrlSplit = originUrl.split("/");
        String lastOriginUrlPart = originUrlSplit[originUrlSplit.length - 1];
        return lastOriginUrlPart.replace(".git", "");

    }

    public Collection<String> getLocalBranchNames() {

        try {

            GitRepositoryManager repositoryManager = GitUtil.getRepositoryManager(this.project);
//        System.out.println(repositoryManager);
            GitMultiRootBranchConfig gitMultiRootBranchConfig = new GitMultiRootBranchConfig(
                    repositoryManager.getRepositories()
            );
//            System.out.println(gitMultiRootBranchConfig.getCommonTrackingBranches());
            System.out.println(gitMultiRootBranchConfig.getLocalBranchNames());
            System.out.println(gitMultiRootBranchConfig.getCurrentBranch());

            Collection<String> localBranchNames = gitMultiRootBranchConfig.getLocalBranchNames();

            System.out.println(localBranchNames.toString());

            return localBranchNames;

        } catch(Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            utils.Notification.notify("getLocalBranchNames Exception", e.getMessage());
        }

        return null;
    }

    public String getBranchName() {

        String currentBranchName = "";

        if (this.repository != null) {
            assert !this.repository.isFresh();
            currentBranchName = this.repository.getCurrentBranchName();
        }

        return currentBranchName;
    }

}
