package utils;

import com.intellij.dvcs.DvcsUtil;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vcs.FilePath;
import com.intellij.openapi.vcs.VcsException;
import com.intellij.openapi.vcs.VcsNotifier;
import com.intellij.openapi.vcs.changes.*;
import com.intellij.openapi.vcs.changes.ui.ChangeNodeDecorator;
import com.intellij.openapi.vcs.changes.ui.ChangesBrowser;
import com.intellij.openapi.vcs.changes.ui.TreeModelBuilder;
import com.intellij.openapi.vcs.history.VcsDiffUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.containers.ContainerUtil;
import com.intellij.vcsUtil.VcsUtil;
import git4idea.GitUtil;
import git4idea.actions.GitCompareWithBranchAction;
import git4idea.branch.GitBrancher;
import git4idea.repo.GitRepository;
import git4idea.repo.GitRepositoryManager;
import git4idea.ui.branch.GitMultiRootBranchConfig;
import gnu.trove.THashSet;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;
import java.util.*;
import java.util.List;

public class Vcs extends GitCompareWithBranchAction {

    private Project project;
    private GitRepository repository;

    public Vcs(Project project, GitRepository repository) {
        this.project = project;
        this.repository = repository;
    }

    JComponent vcsTree;

    public void compareWithBranch(String compareBranchName, JPanel vcsTree) {

        try {

            this.vcsTree = vcsTree;

            com.intellij.openapi.editor.Editor editor =
                    FileEditorManager.getInstance(this.project).getSelectedTextEditor();

            if (editor == null) {
                return;
            }

            Document document = editor.getDocument();

            VirtualFile thisFile = FileDocumentManager.getInstance().getFile(document);

            VirtualFile repositoryRoot = GitUtil.getGitRoot(thisFile);

            if (this.repository == null) {
                return;
            }

            if (this.repository.getCurrentRevision() == null) {
                return;
            }

            String presentableRevisionName = DvcsUtil.getShortHash(this.repository.getCurrentRevision());

            showDiffWithBranchUnderModalProgress(
                    this.project,
                    repositoryRoot,
                    presentableRevisionName,
                    compareBranchName
            );

        } catch (VcsException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            utils.Notification.notify("compareWithBranch", e.getMessage());
        }
    }

    private void showDiffWithBranchUnderModalProgress(@NotNull Project project,
                                                      @NotNull VirtualFile file,
                                                      @NotNull String head,
                                                      @NotNull String compare) {
        new Task.Backgroundable(project, "Collecting Changes...", true) {

            private Collection<Change> changes;

            @Override
            public void run(@NotNull ProgressIndicator indicator) {
                try {
                    this.changes = getDiffChanges(project, file, compare);
                } catch (VcsException e) {
                    VcsNotifier.getInstance(project).notifyImportantWarning("Couldn't compare with branch", String
                            .format("Couldn't compare " + DvcsUtil.fileOrFolder(file) + " [%s] with branch [%s];\n %s", file, compare, e.getMessage()));
                }
            }

            @Override
            public void onSuccess() {
                //if changes null -> then exception occurred before
                if (this.changes != null) {

//                    VcsDiffUtil.showDiffFor(
//                            project,
//                            this.changes,
//                            VcsDiffUtil.getRevisionTitle(compare, false),
//                            VcsDiffUtil.getRevisionTitle(head, true),
//                            VcsUtil.getFilePath(file)
//                    );

                    final ChangesBrowser changesBrowser =
                            new ChangesBrowser(
                                    project,
                                    null,
                                    ContainerUtil.newArrayList(this.changes),
                                    null,
                                    true,
                                    true,
                                    null,
                                    ChangesBrowser.MyUseCase.LOCAL_CHANGES,
                                    null) {
                                @NotNull
                                @Override
                                protected DefaultTreeModel buildTreeModel(List<Change> changes_, ChangeNodeDecorator changeNodeDecorator, boolean showFlatten) {
                                    return TreeModelBuilder.buildFromChanges(myProject, showFlatten, changes, null);
                                }
                            };

                    for(Component c : Vcs.this.vcsTree.getComponents()){
                            Vcs.this.vcsTree.remove(c);
                    }

                    Vcs.this.vcsTree.add(changesBrowser);

                    Vcs.this.vcsTree.revalidate();
                    Vcs.this.vcsTree.repaint();

                } else {
                    System.out.println("No Changes");
                }
            }
        }.queue();
    }

}
