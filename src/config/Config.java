package config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vcs.changes.Change;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

/**
 * PersistentStateComponent keeps project config values.
 * Similar notion of 'preference' in Android
 */
@State(
    name = "GitScope",
    storages = {
        @Storage(
                file="GitScope.xml"
        )
    },
    reloadable=true
)
public class Config implements PersistentStateComponent<Config> {

    /**
     * HOWTO:
     * - Add to plugin.xml: <projectService serviceInterface="config.Config" serviceImplementation="config.Config"/>
     * - To Create a "node" just add class property: public String data = "";
     * -- Implement Getter and Setter
     * --- By using the setter the data is saved
     */

    public String data = "";

    public String history; // JSON Converted JList

    public String dataJson = "";



    public List<String> getHistory() {

//        final DefaultListModel defaultListModel = new DefaultListModel();

        System.out.println("Get historyFromJson");
        System.out.println(this.history);
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(this.history, new TypeToken<List<String>>() {}.getType());

//        // List<String> 2 JLIST
////        JList jlistDefault = new JList(this.defaultListModel);
//        for(String string : historyFromJson){
//            System.out.println(string);
//            defaultListModel.addElement(string);
//        }
//
////        System.out.println(jlistDefault);
//
//        return defaultListModel;
    }

    public void setHistory(JList history) {

        System.out.println("Set history");

        Gson gson = new GsonBuilder().create();

        // JLIST 2 List<String>
        Type listType = new TypeToken<List<String>>() {}.getType();
        List<String> listForJsonExport = new LinkedList<String>();

        ListModel model = history.getModel();

        for(int i=0; i < model.getSize(); i++){
            Object o =  model.getElementAt(i);
            listForJsonExport.add(o.toString());
        }

        String json = gson.toJson(listForJsonExport);
        this.history = json;

    }

    public String getDataJson() {
        return this.dataJson;
    }

    public void setDataJson(String dataJson) {
        this.dataJson = dataJson;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Nullable
    @Override
    public Config getState() {
        return this;
    }

    @Override
    public void loadState(Config config) {
        XmlSerializerUtil.copyBean(config, this);
    }

    @Nullable
    public static Config getInstance(Project project) {
        Config sfec = ServiceManager.getService(project, Config.class);
        return sfec;
    }
}
