package helper;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vcs.changes.Change;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.search.scope.packageSet.NamedScope;
import com.intellij.psi.search.scope.packageSet.NamedScopeManager;

import java.util.Collection;

public class Scope {

    private final Project project;
    private final NamedScopeManager scopeManager;
    private MyPackageSet myPackageSet;

    public Scope(Project project) {

        this.project = project;
        this.scopeManager = NamedScopeManager.getInstance(this.project);

        this.createScope();

    }

    public void createScope() {
        this.myPackageSet = new MyPackageSet();
        NamedScope myScope = new NamedScope("Git Scope (Files)", this.myPackageSet);
        this.scopeManager.removeAllSets();
        this.scopeManager.addScope(myScope);
    }

    public void setScope(Collection<Change> changes){
        this.myPackageSet.setChanges(changes);
    }

}
