package helper;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vcs.VcsException;
import com.intellij.openapi.vfs.VirtualFile;
import git4idea.GitUtil;
import git4idea.repo.*;
import git4idea.ui.branch.GitMultiRootBranchConfig;
import java.util.Collection;

public class GitHelper {

    private Project project;
    private GitRepository repository;

    public GitHelper(Project project) {
        this.project = project;
    }

    private GitRepository receiveRepository() {

        GitRepositoryManager repositoryManager = GitUtil.getRepositoryManager(this.project);

        com.intellij.openapi.editor.Editor editor =
                FileEditorManager.getInstance(this.project).getSelectedTextEditor();

        if (editor == null) {
            return null;
        }

        Document document = editor.getDocument();

        VirtualFile thisFile = FileDocumentManager.getInstance().getFile(document);

        try {

            assert thisFile != null;
            VirtualFile rootGitFile = GitUtil.getGitRoot(thisFile);
            return repositoryManager.getRepositoryForFile(rootGitFile);

        } catch (VcsException e) {
            e.printStackTrace();
        }


        return null;
    }

    public void setRepository (GitRepository repository) {
        this.repository = repository;
    }

    public Collection<String> getLocalBranchNames() {

        try {

            GitRepositoryManager repositoryManager = GitRepositoryManager.getInstance(this.project); //GitUtil.getRepositoryManager(this.project);
            GitMultiRootBranchConfig gitMultiRootBranchConfig = new GitMultiRootBranchConfig(
                    repositoryManager.getRepositories()
            );

            Collection<String> localBranchNames = gitMultiRootBranchConfig.getLocalBranchNames();

            return localBranchNames;

        } catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getBranchName() {

        // Auto-Self-Set Repository
        if (this.repository == null) {
            this.setRepository(this.receiveRepository());
        }

        String currentBranchName = "";

        if (this.repository != null) {
            assert !this.repository.isFresh();
            currentBranchName = this.repository.getCurrentBranchName();
        }

        return currentBranchName;
    }

}
