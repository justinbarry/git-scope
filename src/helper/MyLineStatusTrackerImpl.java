package helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.intellij.dvcs.DvcsUtil;
import com.intellij.openapi.Disposable;
import com.intellij.openapi.editor.*;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.FileEditorManagerEvent;
import com.intellij.openapi.fileEditor.FileEditorManagerListener;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vcs.*;
import com.intellij.openapi.vcs.changes.Change;
import com.intellij.openapi.vcs.changes.ChangeListManager;
import com.intellij.openapi.vcs.changes.ContentRevision;
import com.intellij.openapi.vcs.changes.CurrentContentRevision;
import com.intellij.openapi.vcs.changes.ui.ChangeNodeDecorator;
import com.intellij.openapi.vcs.changes.ui.ChangesBrowser;
import com.intellij.openapi.vcs.changes.ui.TreeModelBuilder;
import com.intellij.openapi.vfs.*;
import com.intellij.util.containers.ContainerUtil;
import config.Config;
import git4idea.GitUtil;
import git4idea.actions.GitCompareWithBranchAction;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;
import java.lang.reflect.Type;
import java.util.*;
import java.util.List;

public class MyLineStatusTrackerImpl extends GitCompareWithBranchAction {

    private final Scope scope;
    private final JLabel status;
    private Project project;
    private VirtualFile repositoryRoot;
    private String branchName;
    private JComponent vcsTree;

    private Collection<Change> changes;

    private List<MyLineStatusTrackerManager> myLineStatusTrackerManagerCollection;

    private Boolean initialized = false;
    private String currentBranch;

    public MyLineStatusTrackerImpl(Project project, JPanel vcsTree, JLabel status) {

        this.project = project;
        this.vcsTree = vcsTree;
        this.status = status;

        this.on();

        // Listen to new opened Tabs
        editorListener();

        init();

        this.scope = new Scope(project);

    }

    private void on() {
        // Deactivate the Main Line Status Manager
        showLstGutterMarkers();
    }

    private void init() {

        com.intellij.openapi.editor.Editor editor =
                FileEditorManager.getInstance(this.project).getSelectedTextEditor();

        if (editor == null) {
            return;
        }

        Document document = editor.getDocument();

        VirtualFile thisFile = FileDocumentManager.getInstance().getFile(document);

        try {

            assert thisFile != null;
            this.repositoryRoot = GitUtil.getGitRoot(thisFile);

            this.initialized = true;
            System.out.println("INITIALIZED!");

        } catch (VcsException e) {
            e.printStackTrace();
        }

    }

    public void compareWithbranch(String compareBranchName, String currentBranch) {

        System.out.println("compareWithbranch " + compareBranchName);

        this.branchName = compareBranchName;
        this.currentBranch = currentBranch;
        doCompare();

    }

    private void doCompare() {

        try {

            if (!this.initialized) {
                this.init();
            }

            if (this.branchName == null) {
                this.branchName = "HEAD";
            }

            String presentableRevisionName = "HEAD";
            if (this.project == null || this.repositoryRoot == null) {
                return;
            }

            System.out.println("doCompare");

            collectChangesAndProcess(
                    this.project,
                    this.repositoryRoot,
                    presentableRevisionName,
                    this.branchName
            );

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateForVirtualFile(VirtualFile vFile) {

        if (this.changes == null) {
            return;
        }

        String updateChangePath = vFile.getPath();

        Boolean add = true;
        for (Change change : this.changes) {
            VirtualFile changeVirtualFile = change.getVirtualFile();
            if (changeVirtualFile == null) {
                continue;
            }
            String changePath = changeVirtualFile.getPath();

            if (updateChangePath.equals(changePath)) {
                add = false;
            }
        }

        if (add) {
            FilePath filePath = new LocalFilePath(vFile.getPath(), false);
            CurrentContentRevision currentContentRevision = new CurrentContentRevision(filePath);
            Change newChange = new Change(null, currentContentRevision);
            this.changes.add(newChange);
        }

        createDiffWindow();
    }

    private Boolean locked = false;

    private void collectChangesAndProcess(@NotNull Project project,
                                          @NotNull VirtualFile file,
                                          @NotNull String head,
                                          @NotNull String compare) {
        new Task.Backgroundable(project, "Collecting Changes...", true) {

            private Collection<Change> changes;

            @Override
            public void run(@NotNull ProgressIndicator indicator) {
                try {
                    if (MyLineStatusTrackerImpl.this.locked) {
                        System.out.println("LOCKED!");
                        return;
                    }
                    MyLineStatusTrackerImpl.this.locked = true;
                    this.changes = getDiffChanges(project, file, compare);
                } catch (VcsException e) {
                    VcsNotifier.getInstance(project).notifyImportantWarning("Couldn't compare with branch", String
                            .format("Couldn't compare " + DvcsUtil.fileOrFolder(file) + " [%s] with branch [%s];\n %s", file, compare, e.getMessage()));
                }
            }

            @Override
            public void onSuccess() {

                MyLineStatusTrackerImpl.this.locked = false;
                ChangeListManager changeListManager = ChangeListManager.getInstance(project);
                Collection<Change> localChanges = changeListManager.getAllChanges();

                for (Change localChange : localChanges) {

                    VirtualFile localChangeVirtualFile = localChange.getVirtualFile();
                    if (localChangeVirtualFile == null) {
                        continue;
                    }
                    String localChangePath = localChangeVirtualFile.getPath();

                    Boolean shouldAdd = true;
                    if (this.changes == null) {
                        continue;
                    }

                    for (Change change : this.changes) {
                        VirtualFile vFile = change.getVirtualFile();
                        if (vFile == null) {
                            continue;
                        }
                        String changePath = change.getVirtualFile().getPath();

                        if (localChangePath.equals(changePath)) {
                            shouldAdd = false;
                        }
                    }

                    if (shouldAdd) {
                        this.changes.add(localChange);
                    }
                }

                if (this.changes != null) {


                    // @todo: Refactor to a class "onChange" (this class should only do comparing an nothing more)

                    MyLineStatusTrackerImpl.this.status.setText(" " + MyLineStatusTrackerImpl.this.currentBranch + " > " + MyLineStatusTrackerImpl.this.branchName);

                    // LineStatusTracker
                    MyLineStatusTrackerImpl.this.loadByRevision(this.changes);

                    // DiffWindow
                    createDiffWindow();

                    // Manage Scopes
                    MyLineStatusTrackerImpl.this.scope.setScope(MyLineStatusTrackerImpl.this.changes);

                    // Write config
                    Config config = Config.getInstance(project);
                    assert config != null;
                    config.setData(this.changes.toString());

                    // yeah JSON too, of course...
                    Gson gson = new GsonBuilder().create();

                    // create a list for json
                    Type listType = new TypeToken<List<String>>() {
                    }.getType();
                    List<String> listForJsonExport = new LinkedList<String>();

                    // ..yes, we need the same loop as some lines before
                    for (Change change : this.changes) {

                        VirtualFile vFile = change.getVirtualFile();
                        if (vFile == null) {
                            continue;
                        }
                        String changePath = vFile.getPath();


                        if (!vFile.isInLocalFileSystem()) {
                            return;
                        }

                        listForJsonExport.add(changePath);
                    }

                    String json = gson.toJson(listForJsonExport);
                    config.setDataJson(json);

                }
            }
        }.queue();
    }

    private void createDiffWindow() {
        // Build the Diff-Tool-Window
        ChangesBrowser changesBrowser =
                new ChangesBrowser(
                        MyLineStatusTrackerImpl.this.project,
                        null,
                        ContainerUtil.newArrayList(this.changes),
                        null,
                        true,
                        true,
                        null,
                        ChangesBrowser.MyUseCase.LOCAL_CHANGES,
                        null) {
                    @NotNull
                    @Override
                    protected DefaultTreeModel buildTreeModel(List<Change> changes_, ChangeNodeDecorator changeNodeDecorator, boolean showFlatten) {
                        return TreeModelBuilder.buildFromChanges(
                                this.myProject,
                                this.myViewer.getGrouping(),
                                MyLineStatusTrackerImpl.this.changes,
                                changeNodeDecorator
                        );
                    }
                };

        for (Component c : MyLineStatusTrackerImpl.this.vcsTree.getComponents()) {
            MyLineStatusTrackerImpl.this.vcsTree.remove(c);
        }

        MyLineStatusTrackerImpl.this.vcsTree.add(changesBrowser);

    }


    private void loadByRevision(Collection<Change> changes) {

        try {

            this.changes = changes;

            releaseAll();

            this.myLineStatusTrackerManagerCollection = new ArrayList<>();

            // Initialize for open Tabs
            initOpenTabs();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void releaseAll() {

        if (this.myLineStatusTrackerManagerCollection == null) {
            return;
        }

        for (Object temp : this.myLineStatusTrackerManagerCollection) {
            MyLineStatusTrackerManager myLineStatusTrackerManager = (MyLineStatusTrackerManager) temp;
            myLineStatusTrackerManager.release();
        }

    }

    private void showLstGutterMarkers() {
        // Deactivate VCS Line Status as it is for now
        VcsApplicationSettings vcsApplicationSettings = VcsApplicationSettings.getInstance();
        vcsApplicationSettings.SHOW_LST_GUTTER_MARKERS = false;
    }

    private void initOpenTabs() {
        Editor[] editors = EditorFactory.getInstance().getAllEditors();
        for (Editor editor : editors) {
            createLineStatusByChangesForEditor(editor);
        }
    }

    private void createLineStatusByChangesForEditor(Editor editor) {

        try {
            // Load Revision

            if (editor == null) {
                return;
            }

            Document doc = editor.getDocument();

            VirtualFile file = FileDocumentManager.getInstance().getFile(doc);
            if (file == null) {
                return;
            }

            // Try to avoid failing...
            // @todo: Refactor
            VirtualFile vcsFile;
            Boolean isOperationUseful = false;
            ContentRevision contentRevision = null;
//            String content = null;
            for (Change change : this.changes) {
                if (change == null) {
                    continue;
                }
                vcsFile = change.getVirtualFile();
                if (vcsFile == null) {
                    continue;
                }

                if (vcsFile.getPath().equals(file.getPath())) {
                    contentRevision = change.getBeforeRevision();
                    isOperationUseful = true;
                    break;
                }
            }

            String content = "";

            if (!isOperationUseful) {
                content = doc.getCharsSequence().toString();
            }

            if (contentRevision != null) {
                content = contentRevision.getContent();
            }

            createLineStatus(editor, content);

        } catch (VcsException ignored) {
        }
    }

    private void createLineStatus(Editor editor, String content) {

        // Create LineStatusTracker
        MyLineStatusTrackerManager myLineStatusTrackerManager = new MyLineStatusTrackerManager(
                this.project,
                editor
        );
        this.myLineStatusTrackerManagerCollection.add(myLineStatusTrackerManager);

        content = StringUtil.convertLineSeparators(content);
        myLineStatusTrackerManager.setBaseRevision(content);

    }

    private void editorListener() {

        this.project.getMessageBus().connect(this.project).subscribe(
                FileEditorManagerListener.FILE_EDITOR_MANAGER,
                new FileEditorManagerListener() {
                    @Override
                    public void fileOpened(@NotNull FileEditorManager source, @NotNull VirtualFile vFile) {
                        doCompare();
                    }

                    @Override
                    public void selectionChanged(@NotNull FileEditorManagerEvent event) {

                    }
                });

        MyFileStatusListenerImpl myFileStatusListenerImpl = new MyFileStatusListenerImpl();
        myFileStatusListenerImpl.listen();
    }


    class MyFileStatusListenerImpl implements Disposable {


        @Override
        public void dispose() {

        }

        void listen() {
            FileStatusListener fileStatusListener = new MyFileStatusListener();
            FileStatusManager.getInstance(MyLineStatusTrackerImpl.this.project).addFileStatusListener(fileStatusListener, this);
        }

        private final class MyFileStatusListener implements FileStatusListener {
            @Override
            public void fileStatusesChanged() {

                if (!MyLineStatusTrackerImpl.this.initialized) {
                    return;
                }

                doCompare();
            }

            @Override
            public void fileStatusChanged(@NotNull VirtualFile vFile) {

                if (!MyLineStatusTrackerImpl.this.initialized) {
                    return;
                }

                updateForVirtualFile(vFile);

            }
        }
    }
}