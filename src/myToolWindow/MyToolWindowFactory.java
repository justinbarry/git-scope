package myToolWindow;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.*;
import com.intellij.ui.content.*;

import javax.swing.*;

import helper.GitHelper;
import helper.MyLineStatusTrackerImpl;

/**
 * MyToolWindowFactory
 *
 * This is actually the "main" - Class
 */
public class MyToolWindowFactory implements ToolWindowFactory {

    private ToolWindow toolWindow;
    private JPanel myToolWindowContent;
    private MyToolWindowUI myToolWindowUI;

    public MyLineStatusTrackerImpl myLineStatusTrackerImpl;
    public Project project;
    public JComboBox revisionComboBox;

    public JPanel vcsTree;
    public JList favlist;
    public JCheckBox originCheckBox;
    public JCheckBox headCheckBox;
    public JLabel status;

    /**
     * Create Tool Window Content - Intellij Hook - on open/initGitRepositoryOnStart ToolWindow each instance
     * @param project
     * @param toolWindow
     */
    public void createToolWindowContent(Project project, ToolWindow toolWindow) {
        //instantiate the class itself
        MyToolWindowFactory myToolWindowFactory = new MyToolWindowFactory();
        myToolWindowFactory.process(project, toolWindow);
    }

    /**
     * Create UI Components - Intellij Hook - Create Webview Panel in ToolWindow
     */
    private void createUIComponents() {
        this.myToolWindowUI = new MyToolWindowUI(this);
        this.myToolWindowUI.beforInit();
    }

    /**
     * Process after Initiating
     * @param project
     * @param toolWindow
     */
    private void process(Project project, ToolWindow toolWindow){

        // Set Project
        this.project = project;

        // Set ToolWindow
        this.toolWindow = toolWindow;

        // Create ToolWindow
        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        Content content = contentFactory.createContent(this.myToolWindowContent, "", false);
        this.toolWindow.getContentManager().addContent(content);

        // LineStatusTracker
        this.myLineStatusTrackerImpl = new MyLineStatusTrackerImpl(project, this.vcsTree, this.status);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // GitHelper
                GitHelper gitHelper = new GitHelper(MyToolWindowFactory.this.project);

                // After Init
                MyToolWindowFactory.this.myToolWindowUI.afterInit(gitHelper);
            }
        });


    }

}

