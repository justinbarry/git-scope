package myToolWindow;

import com.intellij.openapi.project.Project;
import com.intellij.ui.components.JBList;
import config.Config;
import git4idea.repo.GitRepository;
import git4idea.repo.GitRepositoryChangeListener;
import org.jetbrains.annotations.NotNull;
import helper.GitHelper;

import javax.swing.*;
import javax.swing.text.Position;
import java.awt.*;
import java.util.*;
import java.util.List;

public class MyToolWindowUI {

    MyToolWindowFactory myToolWindowFactory;
    GitHelper gitHelper;
    String HEAD = "HEAD";
    Collection<String> localBranchNamesCache;
    Object selectedItem;
    String selectedItemValue;
    Integer lastFavListSelectionIdx = null;
    Integer preSelectIndex = null;
    Boolean comboboxListenerOn = false;
    private Config config;
    private String currentTargetBranch;

    public MyToolWindowUI(MyToolWindowFactory myToolWindowFactory) {
        this.myToolWindowFactory = myToolWindowFactory;
    }

    public void beforInit() {

        this.initFavList();

        this.myToolWindowFactory.vcsTree = new JPanel();
        this.myToolWindowFactory.vcsTree.setLayout(new BorderLayout());
    }

    public void afterInit(GitHelper gitHelper) {

//        MyToolWindowUI.this.myToolWindowFactory.favListPane.setLayout(new ScrollPaneLayout());
//        MyToolWindowUI.this.myToolWindowFactory.favListPane.setVisua


        this.gitHelper = gitHelper;
        this.gitRepoListenerForComboxbox();

        this.myToolWindowFactory.revisionComboBox.setEditable(true);

        this.populateCombobox();

        this.originCheckboxListener();

        this.headCheckboxListener();

        this.config = Config.getInstance(this.myToolWindowFactory.project);
        this.favListListener();

        this.comboboxListener();
        this.comboboxListenerOn = true;

        // We start with HEAD per default
        this.myToolWindowFactory.headCheckBox.setSelected(true);

        System.out.println("after Init done!");

        doCompare(this.HEAD);

    }

    private Boolean doCompareAfterRepositoryChanged = false;

    private void gitRepoListenerForComboxbox() {
        this.myToolWindowFactory.project.getMessageBus().connect().subscribe(GitRepository.GIT_REPO_CHANGE, new GitRepositoryChangeListener() {
            @Override
            public void repositoryChanged(@NotNull GitRepository repositoryNew) {

                Project project = repositoryNew.getProject();
                if (project.isDisposed()) {
                    return;
                }

                MyToolWindowUI.this.gitHelper.setRepository(repositoryNew);

                System.out.println("repositoryChanged");
                MyToolWindowUI.this.comboboxListenerOn = false;
                MyToolWindowUI.this.populateCombobox();
                if (MyToolWindowUI.this.doCompareAfterRepositoryChanged) {
                    if (MyToolWindowUI.this.currentTargetBranch != null) {
                        doCompare(MyToolWindowUI.this.currentTargetBranch);
                    } else {
                        doCompare(MyToolWindowUI.this.HEAD);
                    }
                } else {
                    doCompare(MyToolWindowUI.this.HEAD);
                }
                MyToolWindowUI.this.doCompareAfterRepositoryChanged = true;
                MyToolWindowUI.this.comboboxListenerOn = true;
            }
        });
    }

    DefaultListModel historyList = new DefaultListModel();

    private void initFavList() {

        JList list = new JBList(this.historyList);

        this.setFavList(list);

    }

    private void setFavList(JList list) {
        MyToolWindowUI.this.myToolWindowFactory.favlist = list;
    }

    private void populateCombobox() {

        JComboBox combobox = MyToolWindowUI.this.myToolWindowFactory.revisionComboBox;

        // Update Necessary?
        Collection<String> localBranchNames = this.gitHelper.getLocalBranchNames();
        if (localBranchNames.equals(this.localBranchNamesCache)) {
            return;
        }
        this.localBranchNamesCache = localBranchNames;

        // Remove all items
        if (combobox != null && combobox.getItemCount() > 0) {
            combobox.removeAllItems();
        }

        // Add new items with "localBranchNames"
//        combobox.addItem(this.HEAD);
        for (String localBranchName : localBranchNames) {
            combobox.addItem(localBranchName);
        }

        if (this.selectedItem != null) {
            combobox.setSelectedItem(this.selectedItem);
        }
    }

    private void comboboxListener() {
        this.myToolWindowFactory.revisionComboBox.addActionListener(e -> {
            if (!this.comboboxListenerOn) {
                return;
            }
            this.myToolWindowFactory.headCheckBox.setSelected(false);
            this.doCompareAccordingToCombobox();
        });
    }

    private void originCheckboxListener() {
        this.myToolWindowFactory.originCheckBox.addActionListener(e -> {
            this.myToolWindowFactory.headCheckBox.setSelected(false);
            doCompareAccordingToCombobox();
        });
    }

    private void headCheckboxListener() {
        this.myToolWindowFactory.headCheckBox.addActionListener(e -> {
            if (this.myToolWindowFactory.headCheckBox.isSelected()) {
                doCompare(this.HEAD);
                this.blockCompare = true;
                this.myToolWindowFactory.favlist.clearSelection();
                this.blockCompare = false;
            } else {
                if (this.lastFavListSelectionIdx == null) {
                    this.doCompareAccordingToCombobox();
                }else {
                    this.myToolWindowFactory.favlist.setSelectedIndex(this.lastFavListSelectionIdx);
                    this.doCompare(this.myToolWindowFactory.favlist.getSelectedValue().toString());
                }
            }
        });
    }

    private void favListListener() {

        List<String> historyFromJson = this.config.getHistory();

        if (historyFromJson != null) {
            for (String string : historyFromJson) {
                System.out.println(string);
                this.historyList.addElement(string);
            }
        }

        this.myToolWindowFactory.favlist.addListSelectionListener(e -> {

            if (!e.getValueIsAdjusting()) {
                return;
            }

            if (this.myToolWindowFactory.favlist == null) {
                return;
            }

            Object favListValue = this.myToolWindowFactory.favlist.getSelectedValue();
            if (favListValue == null) {
                return;
            }

            this.lastFavListSelectionIdx = this.myToolWindowFactory.favlist.getSelectedIndex();

            String branch = favListValue.toString();

            this.doCompare(branch);
            this.myToolWindowFactory.headCheckBox.setSelected(false);

            this.config.setHistory(this.myToolWindowFactory.favlist);

        });
    }

    private Boolean blockCompare = false;

    private void doCompareAccordingToCombobox() {

        if (this.blockCompare) {
            return;
        }

        if (this.myToolWindowFactory.revisionComboBox == null) {
            return;
        }

        this.selectedItem = this.myToolWindowFactory.revisionComboBox.getSelectedItem();

        if (this.selectedItem == null) {
            return;
        }

        this.selectedItemValue = this.selectedItem.toString();

        if (this.myToolWindowFactory.originCheckBox.isSelected()) {
            this.selectedItemValue = "origin/" + this.selectedItemValue;
        }

        if (this.myToolWindowFactory.headCheckBox.isSelected()) {
            doCompare(this.HEAD);
            return;
        }

        if (this.selectedItemValue == null) {
            return;
        }

        if (this.historyList.indexOf(this.selectedItemValue) == -1) {
            this.historyList.addElement(this.selectedItemValue);
        }
        if (this.historyList.indexOf(this.selectedItemValue) == 4) {
            this.historyList.removeElementAt(0);
        }

        String s = this.selectedItemValue;
        try {
            int index = this.myToolWindowFactory.favlist.getNextMatch(s, 0, Position.Bias.Forward);
            if (index != -1)
                this.myToolWindowFactory.favlist.setSelectedValue(s, true);
        } catch(Exception e) {

        }

        doCompare(this.selectedItemValue);
    }

    private void doCompare(String branch) {

        this.currentTargetBranch = branch;

        String currentBranch = this.gitHelper.getBranchName();
        System.out.println(currentBranch + " > " + branch);
        this.myToolWindowFactory.status.setText("loading...");

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MyToolWindowUI.this.myToolWindowFactory.myLineStatusTrackerImpl.compareWithbranch(branch, currentBranch);
            }
        });
    }
}
